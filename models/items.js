const { Schema, model, Types } = require("mongoose")

const schema = new Schema({
    name: { type: String, required: true },
    collectionId: { type: String, required: true },
    fields: []
})
module.exports = model("Items", schema)