const { Router } = require("express")
const router = Router()
const User = require("../models/user")
const bcrypt = require("bcryptjs")
const { check, validationResult } = require("express-validator")
const jwt = require("jsonwebtoken")

router.post("/register",
    [
        check("email", "Wrong email").isEmail(),
        check("password", "Number of simbol must be 6")
            .isLength({ min: 6 })



    ],


    async (req, res) => {

    try {

        console.log("body: ", req.body)
        const errors = validationResult(req)
        if (!errors.isEmpty()) {
            return res.status(400).json({
                errors: errors.array(),
                message: "Wrong date on register"
            })
        }


        const { email, password } = req.body

        const candidate = await User.findOne({ email:email })

        if (candidate) {
            return res.status(400).json({ message: "This user is already exist.." })
        }

        const hashedPassword = await bcrypt.hash(password, 12)

        const user = new User({ email: email, password: hashedPassword })

        await user.save()

        res.status(201).json({ message: "User was created" })


    } catch (e) {
        res.status(500).json({message: "Try again..."})
    }
})


router.post("/login",
    [
        check("email", "Wrong email").normalizeEmail().isEmail(),
        check("password", "input password")
            .exists()


    ],

    

    async (req, res) => {

        try {

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                    message: "Wrong date on signin"
                })
            }


            const { email, password } = req.body

            const user = await User.findOne({ email })

            if (!user) {
                return res.status(400).json({
                    message: "No such user"
                })

            }

            const isMatch = await bcrypt.compare(password, user.password)

            if (!isMatch) {
                return res.status(400).json({
                    message: "Wrong password"
                })
            }

          
            const token = jwt.sign(
                { userId: user.id },
                "what your whant",
                {expiresIn: "1h"}
            )
            
                
            res.json({ token, userId: user.id })

        } catch (e) {
            res.status(500).json({ message: "Try again..." })
        }

    }
)



module.exports = router