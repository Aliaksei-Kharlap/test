const { Schema, model, Types } = require("mongoose")

const schema = new Schema({
    name: { type: String, required: true },
    description: { type: String, required: true },
    topic: { type: String, required: true },
    fields: [],
    owner: { type: Types.ObjectId, ref: "User"}
})
module.exports = model("Collection", schema)