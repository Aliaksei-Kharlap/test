const { Router } = require("express")
const router = Router()
const Items = require("../models/items")
const Collection = require("../models/collection")
const User = require("../models/user")
const { check, validationResult } = require("express-validator")
const auth = require("../middleware/auth.middleware")
const shortId = require("shortid")



router.post("/generate", auth, async (req, res) => {


    try {

        const { name, description, topic, fields  } = req.body

        

        const link = new Collection({
            name, description, topic, fields, owner: req.user.userId
        })

        await link.save()


        res.status(201).json({ link })

    } catch (e) {
        res.status(500).json({ message: "Try again..." })
    }


})


router.post("/items", auth, async (req, res) => {


    try {

        const { name, collectionId, fields } = req.body


        const link = new Items({
            "name": name, "collectionId": collectionId.linkId, "fields": fields
        })

        await link.save()

        

        res.status(201).json({ link })

    } catch (e) {
        res.status(500).json({ message: "Try again..." })
    }


})

router.get("/", auth, async (req, res) => {

    try {

        const links = await Collection.find({ owner: req.user.userId })

        
        res.json(links)
        

    } catch (e) {
        res.status(500).json({ message: "Try again..." })
    }

})

router.get("/such/:id", auth, async (req, res) => {

    try {

        const coll = await Collection.findById(req.params.id)
        const items = await Items.find({ "collectionId": req.params.id })

        const a = [coll, items]

        
        res.json(a)


    } catch (e) {
        res.status(500).json({ message: "Try again..." })
    }


})

router.get("/:id", auth, async (req, res) => {

    try {

        const coll = await Collection.findById(req.params.id)

        res.json(coll)


    } catch (e) {
        res.status(500).json({ message: "Try again..." })
    }


})


module.exports = router